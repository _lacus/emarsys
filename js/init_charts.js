$(function() {
  // Create the chart
  for(var i = 1; i < 6; i++){
    var chart = new Highcharts.Chart({
      chart: {
        renderTo: 'panel_pie_' + i,
        type: 'pie'
      },
      title: {
        text: ''
      },
      yAxis: {
        title: {
          text: 'Total percent market share'
        }
      },
      plotOptions: {
        pie: {
          shadow: false
        }
      },
      tooltip: {
        formatter: function() {
          return this.y +' %';
        }
      },
      series: [{
        name: 'Browsers',
        data: [["A",25],["B",30]],
        size: '195%',
        innerSize: '80%',
        showInLegend:false,
        dataLabels: {
          enabled: false
        }
      }]
    });
    text = chart.renderer.text("25%").add();
    textBBox = text.getBBox();
    x = chart.plotLeft + (chart.plotWidth  * 0.5) - (textBBox.width  * 0.5);
    y = chart.plotTop  + (chart.plotHeight * 0.5) - (textBBox.height * 0.5);
    text.attr({x: '33%', y: '56%'});
  }
});

$(function() {
  // Create the chart
  for(var i = 1; i < 10; i++){
    var container = '#panel_chart_' + i;
    $(container).highcharts({
      type: 'area',
      title: null,
      series: [{
        data: [0.1, 0.15, 0.17, 0.09, 0.1, 0.15, 0.17, 0.09, 0.1, 0.15, 0.17, 0.09, 0.1, 0.15, 0.17, 0.09, 0.1, 0.15, 0.17, 0.09, 0.1, 0.15, 0.17, 0.09, 0.1, 0.15, 0.17, 0.09]
      }],
      xAxis: {
        lineWidth: 0,
        minorGridLineWidth: 0,
        lineColor: 'transparent',
        labels: {
          enabled: false
        },
        minorTickLength: 0,
        tickLength: 0,
        gridLineColor: 'transparent'
      },
      yAxis: {
        lineWidth: 0,
        minorGridLineWidth: 0,
        lineColor: 'transparent',
        labels: {
          enabled: false
        },
        minorTickLength: 0,
        tickLength: 0,
        gridLineColor: 'transparent'
      }
    });
  }
});
