$(document).ready( function() { 
	$('.dashboard_panel').append('<i class="switch fa fa-angle-double-down"></i>');
	$('.switch').click(function() {
		$(this).toggleClass('fa-angle-double-down fa-angle-double-up');
		$(this).parent('section').toggleClass('open');
	});
	$('.tooltip').click(function() {
		$(this).toggleClass('open');
	});
});
